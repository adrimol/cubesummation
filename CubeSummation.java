import java.io.*;
import java.util.*;

public class CubeSummation {
    long[][][] arbolFenwick;
    long[][][] matrizNum;
    private int dimensiones = 0;
    
    public CubeSummation(int dimensiones) {
        if (dimensiones == 0) 
        	return;
        this.dimensiones = dimensiones;
        arbolFenwick = new long[dimensiones+1][dimensiones+1][dimensiones+1];
        matrizNum = new long[dimensiones][dimensiones][dimensiones];
    }
    
    public void actualizar(int x, int y, int z, int value) {
        long delta = value - matrizNum[x][y][z];
        matrizNum[x][y][z] = value;
        for (int i = x + 1; i <= dimensiones; i += i & (-i)) {
            for (int j = y + 1; j <= dimensiones; j += j & (-j)) {
                for (int k = z + 1; k <= dimensiones; k += k & (-k)) {
                    arbolFenwick[i][j][k] +=  delta;
                }
            }
        }
    }
    
    public void consultar(int x1, int y1, int z1, int x2, int y2, int z2) {
        long resultado = sumar(x2+1,y2+1,z2+1) - sumar(x1,y1,z1) - sumar(x1,y2+1,z2+1) - sumar(x2+1,y1,z2+1) - sumar(x2+1,y2+1,z1) + sumar(x1,y1,z2+1) + sumar(x1,y2+1,z1) + sumar(x2+1,y1,z1);
        System.out.println(resultado);
    }
    
    public long sumar(int x, int y, int z) {
        long suma = 0l;
        for (int i = x; i > 0; i -= i & (-i)) {
            for (int j = y; j > 0; j -= j & (-j)) {
                for (int k = z; k > 0; k -= k & (-k)) {
                    suma += arbolFenwick[i][j][k];
                }
            }
        }
        return suma;
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner ob = new Scanner(System.in);
        CubeSummation cuboSum = null;
        int testcases = ob.nextInt();
        ob.nextLine();
        for (int i = 0; i < testcases; i++) {
            String numsLine = ob.nextLine();
            String[] numsLineParts = numsLine.trim().split(" ");
            int dimensiones = Integer.valueOf(numsLineParts[0]);
            int numOperaciones = Integer.valueOf(numsLineParts[1]);
            cuboSum = new CubeSummation(dimensiones);
            for (int j = 0; j < numOperaciones; j++) {
                String line = ob.nextLine();
                String[] lineParts = line.split(" ");
                if (lineParts[0].equals("UPDATE")) {
                    cuboSum.actualizar(Integer.valueOf(lineParts[1])-1, Integer.valueOf(lineParts[2])-1, Integer.valueOf(lineParts[3])-1, Integer.valueOf(lineParts[4]));
                }
                if (lineParts[0].equals("QUERY")) {
                    cuboSum.consultar(Integer.valueOf(lineParts[1])-1, Integer.valueOf(lineParts[2])-1, Integer.valueOf(lineParts[3])-1, 
                    					Integer.valueOf(lineParts[4])-1, Integer.valueOf(lineParts[5])-1, Integer.valueOf(lineParts[6])-1);
                }
            }
        }
    }
}  

